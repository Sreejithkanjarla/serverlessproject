import React from 'react';
import axios from 'axios';


export default class LoginPage extends React.Component {

    constructor (props) {

        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit (e) {

        e.preventDefault();

        let requestForApi = {

            email: document.getElementById('exampleInputEmail1').value ,
            password: document.getElementById('exampleInputPassword1').value,

        }

        axios.post("https://qr2ig21610.execute-api.ap-south-1.amazonaws.com/login", requestForApi)
        .then(resp => {
            alert(resp.data.message);
        })
        .catch(err => {
            alert(err);
            console.log(err);
        })

    }

    render () {

        return <div class="container">
            <div className= "row">
            <div class="col-3"></div>
            <form 
                onSubmit={(e) => this.onSubmit(e)} 
                class="my-5 p-5 col-6 bg-light border-light rounded">
                <div class="form-group my-2">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group my-2">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>

                <button type="submit" class="btn btn-primary my-4">Submit</button>
            </form>
            <div class="col-3"></div>
            </div>
        </div> 

    }

}  

