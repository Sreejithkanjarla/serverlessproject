import React from 'react';
import LoginPage from './components/Loginpage';

export default class App extends React.Component {
  
  render () {

    return (
      <div className="App">
        <LoginPage />
      </div>
    );

  }

}
